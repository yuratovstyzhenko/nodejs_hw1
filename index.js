const http = require('http');
const fs = require('fs');
const url = require('url');

module.exports = () => {
    if(!fs.existsSync('./file/')) {
        fs.mkdirSync('./file');
    }
    http.createServer(function(request, response){
        const { pathname, query } = url.parse(request.url, true);
        function loger(log) {
            log.time = Date.now();
            fs.readFile('./logs.json', (err, data) => {
                if(err) {
                    response.writeHead(400, {'Content-type' : 'text/html'});
                    response.end('Mistake while reading log file was found');
                } else {
                    let object;
                    if(data.toString() !== '') {
                        object = JSON.parse(data);
                    } else {
                        object = {
                            "logs": []
                        };
                    }
                    object.logs.push(log);
                    fs.writeFile('./logs.json', JSON.stringify(object), (err) => {
                        if(err) {
                            response.writeHead(400, {'Content-type' : 'text/html'});
                            response.end('Mistake while logging was found');
                        }
                    });
                }
            });
        }

        if(request.method === 'GET') {
            if(pathname === '/logs') {
                fs.readFile(`.${pathname}.json`, (err, content) => {
                    if(err) {
                        response.writeHead(400, {'Content-type' : 'text/html'});
                        response.end('Log file hasn\`t found');
                    } else {
                        loger({
                            message: `Geting logs`
                        });
                        let object = JSON.parse(content);
                        if(query.from) {
                            object.logs = object.logs.filter( el => {
                                if(el.time >= +query.from) {
                                    return el;
                                }
                            });
                        } 
                        if(query.to) {
                            object.logs = object.logs.filter( el => {
                                if(el.time <= +query.to) {
                                    return el;
                                }
                            });
                        }
                        response.writeHead(200, {'Content-type' : 'application/json'});
                        response.end(JSON.stringify(object));
                    }
                });
            } else {
                fs.readFile(`.${pathname}/${query.filename}`, (err, content) => {
                    if(err) {
                        response.writeHead(400, {'Content-type' : 'text/html'});
                        response.end(`There are no files with ${query.filename} name found`);
                    } else {
                        loger({
                            message: `Reading file with name ${query.filename}`
                        });
                        response.writeHead(200, {'Content-type' : 'text/html'});
                        response.end(content);
                    }
                });
            }
        } else {
            if (!query.filename || !query.content) {
                response.writeHead(400, {'Content-type': 'text/html'});
                response.end(`Unappropriate parameters given`);
            }
            fs.writeFile(`.${pathname}/${query.filename}`, query.content, {flag: 'a'}, (err) => {
                if(err) {
                    response.writeHead(400, {'Content-type' : 'text/html'});
                    response.end(`Mistake while writing ${query.filename} file was found`);
                } else {
                    loger({
                        message: `New file with name ${query.filename} was saved`
                    });
                    response.writeHead(200, {'Content-type' : 'text/html'});
                    response.end('Success');
                }
            });
        }

    }).listen(process.env.PORT || 8080);
}